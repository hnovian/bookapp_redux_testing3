jest.mock('react-native/Libraries/EventEmitter/NativeEventEmitter');

function validation(testName) {
  if (testName.length === 0) {
    return false;
  }
  if (typeof testName === 'string') {
    return true;
  }
  return false;
}

function isValidPassword(testPassword) {
  const regex = /(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])/;
  if (testPassword.length === 0) {
    return false;
  }
  if (typeof testPassword === 'string') {
    if (regex.test(testPassword) && testPassword.length >= 8) {
      return true;
    }
    return false;
  }
  return false;
}

function checkEmail(testEmail) {
  const regex = /^([a-zA-Z0-9])(\.?[a-zA-Z0-9]){5,29}@([a-zA-Z]+)\.([a-zA-Z]{2,3})(\.[a-zA-Z]{2,3})?$/; // pola email
  if (regex.test(testEmail) && typeof testEmail === 'string') {
    return true;
  } if (testEmail.length === 0) {
    return false;
  }
  const reg = /(?=.*[@])/;
  if (!reg.test(testEmail)) {
    return false;
  }
  return false;
}

describe('validation', () => {
  it('should return true', () => {
    expect(validation('test')).toBe(true);
  });
  it('should return false', () => {
    expect(validation('')).toBe(false);
  });
  it('should return false', () => {
    expect(validation(1)).toBe(false);
  });
});

describe('isValidPassword', () => {
  it('should return true', () => {
    expect(isValidPassword('Test12345')).toBe(true);
  });
  it('should return false', () => {
    expect(isValidPassword('')).toBe(false);
  });
  it('should return false', () => {
    expect(isValidPassword(1)).toBe(false);
  });
});

describe('checkEmail', () => {
  it('should return true', () => {
    expect(checkEmail('cobacoba@gmail.com')).toBe(true);
  });
  it('should return false', () => {
    expect(checkEmail('')).toBe(false);
  });
  it('should return false', () => {
    expect(checkEmail(1)).toBe(false);
  });
});
