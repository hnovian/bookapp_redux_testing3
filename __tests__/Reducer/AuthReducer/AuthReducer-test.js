import { authReducers } from '../../../src/redux/reducer';
import {
  LOGIN, LOGIN_LOADING, REGISTER, REGISTER_LOADING, LOGOUT,
} from '../../../src/redux/types';

describe('authReducers', () => {
  it('should return the initial state', () => {
    expect(authReducers(undefined, {})).toEqual({
      token: null,
      name: '',
      isLoading: false,
    });
  });
  describe('Login', () => {
    it('should handle LOGIN', () => {
      expect(authReducers({}, {
        type: LOGIN,
        payload: 'token',
        name: 'name',
      })).toEqual({
        token: 'token',
        name: 'name',
        isLoading: false,
      });
    });

    it('should handle LOGIN_LOADING', () => {
      expect(authReducers({}, {
        type: LOGIN_LOADING,
        payload: true,
      })).toEqual({
        isLoading: true,
      });
    });
  });

  describe('Register', () => {
    it('should handle REGISTER', () => {
      expect(authReducers({}, {
        type: REGISTER,
      })).toEqual({
        isLoading: false,
      });
    });

    it('should handle REGISTER_LOADING', () => {
      expect(authReducers({}, {
        type: REGISTER_LOADING,
        payload: true,
      })).toEqual({
        isLoading: true,
      });
    });
  });

  describe('Logout', () => {
    it('should handle LOGOUT', () => {
      expect(authReducers({}, {
        type: LOGOUT,
      })).toEqual({
        token: null,
        name: '',
      });
    });
  });
});
