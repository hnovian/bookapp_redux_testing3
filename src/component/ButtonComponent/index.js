import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { colors, fonts } from '../../utils';

function ButtonComponent({ title, onPress, mediaHandling }) {
  if (mediaHandling) {
    return (
      <Button
        buttonStyle={styles.media}
        titleStyle={styles.mediaText}
        title={title}
        onPress={onPress}
      />
    );
  }
  return <Button buttonStyle={styles.button} title={title} onPress={onPress} />;
}

export default ButtonComponent;

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.background.secondary,
    borderRadius: 10,
    paddingVertical: 12,
  },
  media: {
    height: 30,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',

  },
  mediaText: {
    fontFamily: fonts.primary[600],
    fontSize: 10,
  },
});
