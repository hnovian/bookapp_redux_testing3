import React from 'react';
import {
  Dimensions, StyleSheet, Text, View,
} from 'react-native';
import { Rating } from 'react-native-elements';
import { colors, fonts } from '../../utils';
import ButtonComponent from '../ButtonComponent';

function CardDetailSale({ rating, totalSale, harga }) {
  const hargaConvert = `Rp. ${parseFloat(harga).toLocaleString('id-ID')}`;
  return (
    <View style={styles.card}>
      <View style={styles.ratingWrapper}>
        <Text style={styles.labelDetail}>Rating</Text>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={styles.labelDescription}>{rating}</Text>
          <Rating
            imageSize={12}
            ratingCount={1}
            readOnly
            startingValue={1}
          />
        </View>

      </View>
      <View style={styles.totalSaleWrapper}>
        <Text style={styles.labelDetail}>Total Sale</Text>
        <Text style={styles.labelDescription}>{totalSale}</Text>
      </View>
      <View style={styles.buttonWrapper}>
        <ButtonComponent title={hargaConvert} />
      </View>
    </View>
  );
}

export default CardDetailSale;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: windowHeight * 0.10,
    width: windowWidth * 0.90,
    alignItems: 'center',
    backgroundColor: colors.background.primary,
    elevation: 6,
    borderRadius: 10,
    paddingHorizontal: 13,
  },
  ratingWrapper: {
    flex: 2,
    alignItems: 'center',
  },
  totalSaleWrapper: {
    flex: 2,
    alignItems: 'center',
  },
  buttonWrapper: {
    flex: 3,
  },
  labelDetail: {
    fontSize: 14,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  labelDescription: {
    fontSize: 14,
    fontFamily: fonts.primary[600],
    color: colors.text.rating,
  },

});
