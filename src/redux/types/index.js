// Loading
export const SET_LOADING = '@SET_LOADING';

// Register
export const REGISTER = '@REGISTER';
export const REGISTER_LOADING = '@REGISTER_LOADING';

// Login
export const LOGIN = '@LOGIN';
export const LOGIN_LOADING = '@LOGIN_LOADING';

// Books
export const GET_BOOKS_RECOMMENDED = '@GET_BOOKS_RECOMMENDED';

export const GET_BOOKS_POPULAR = '@GET_BOOKS_POPULAR';

export const GET_BOOKS_ID = '@GET_BOOKS_ID';

// offline
export const SET_ONLINE = '@SET_ONLINE';

// Logout
export const LOGOUT = '@LOGOUT';

// SET_REFRESHING
export const SET_REFRESHING = '@SET_REFRESHING';
