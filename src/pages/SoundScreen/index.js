import { StyleSheet, View, TouchableOpacity } from 'react-native';
import React, { useEffect, useState } from 'react';
import Sound from 'react-native-sound';
import Ionicons from 'react-native-vector-icons/Ionicons';
import tones from '../../assets/music/Tones.mp3';

function SoundScreen() {
  const audio = new Sound(
    tones,

  );
  const [playing, setPlaying] = useState(false);

  useEffect(() => {
    Sound.setCategory('Playback', true); // true = mixWithOthers
    audio.setVolume(1);
    return () => {
      audio.release();
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const playPause = () => {
    if (audio.isPlaying()) {
      setPlaying(false);
      audio.pause();
    } else {
      setPlaying(true);
      audio.play();
    }
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.playBtn} onPress={playPause}>
        <Ionicons
          name={playing ? 'ios-pause-outline' : 'ios-play-outline'}
          size={36}
          color="#fff"
        />
      </TouchableOpacity>
    </View>
  );
}

export default SoundScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
  playBtn: {
    padding: 20,
  },
});
