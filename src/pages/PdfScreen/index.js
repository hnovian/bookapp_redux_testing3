import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import Pdf from 'react-native-pdf';
import { colors } from '../../utils';
import { IconButton } from '../../component';

function PdfScreen({ navigation }) {
  return (
    <View style={styles.page}>
      <IconButton type="back" onPress={() => navigation.goBack()} />
      <View style={styles.pdfWrapper}>
        <Text style={{ color: 'white', marginBottom: 10 }}>
          My Document Project
        </Text>
        <Pdf
          source={{
            uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf',
          }}
          style={styles.pdf}
        />
      </View>
    </View>
  );
}

export default PdfScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
    padding: 12,
  },
  pdf: {
    width: '100%',
    height: '100%',

  },
  pdfWrapper: {
    width: '100%',
    height: '100%',
  },
});
